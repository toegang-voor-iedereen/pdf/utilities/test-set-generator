export default {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: 'tsconfig.json',
    },
    sourceType: 'module',
    ecmaVersion: 'latest',
    ignorePatterns: ['**/*.js', 'dist', 'node_modules', 'coverage', '.vscode', 'jest.config.ts'],
    reportUnusedDisableDirectives: true,
    plugins: ['@typescript-eslint', 'import'],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:prettier/recommended',
        'prettier',
    ],
    rules: {
        '@typescript-eslint/no-floating-promises': 'error',
    },
    settings: {
        'import/resolver': {
            typescript: {},
        },
        'import/external-module-folders': ['node_modules', 'typings'],
    },
    overrides: [],
}
