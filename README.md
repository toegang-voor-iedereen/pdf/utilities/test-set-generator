# Test set generator

This application downloads PDF files to your disk or uploads them directly into a Minio bucket.

Just clone this repo, run `npm install` and run `npm run get`. You will be walked through downloading your documents!