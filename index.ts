import 'dotenv/config'
import got, { Response } from 'got'
import inquirer, { Answers, DistinctQuestion } from 'inquirer'
import * as minio from 'minio'
import fs from 'node:fs'
import path from 'node:path'
import { pipeline as streamPipeline } from 'node:stream/promises'
import ora from 'ora'
import { z } from 'zod'

let { MINIO_HOST, MINIO_PORT, MINIO_ACCESS_KEY, MINIO_SECRET_KEY } = process.env

const downloadDir = './downloads'

const PublicationNames = [
    'Gemeenteblad',
    'Staatscourant',
    'Tractatenblad',
    'Provinciaal blad',
    'Waterschapsblad',
    'Blad gemeenschappelijke regeling',
    'Agenda',
    'Handelingen',
    'Kamerstuk',
    'Kamervragen (Aanhangsel)',
    'Kamervragen zonder antwoord',
] as const

const PublicationSubCategories = ['Motie'] as const

const prompts: DistinctQuestion<Answers>[] = [
    {
        type: 'list',
        name: 'mode',
        message: 'Do you want to download documents or push them to MinIO?',
        choices: ['minio', 'download'],
        default: 'minio',
    },
    {
        type: 'input',
        name: 'minioHost',
        message:
            "Let's get started! It seems you have not setup MinIO yet... What is your MinIO host? (e.g 127.0.0.1)",
        default: '127.0.0.1',
        when: (answers) => (answers['mode'] === 'minio' && !MINIO_HOST ? true : false),
    },
    {
        type: 'input',
        name: 'minioPort',
        message: 'What is your MinIO port? (e.g 9000)',
        default: '9000',
        when: (answers) => (answers['mode'] === 'minio' && !MINIO_PORT ? true : false),
    },
    {
        type: 'input',
        name: 'minioAccessKey',
        message: 'What is your MinIO access key?',
        when: (answers) => (answers['mode'] === 'minio' && !MINIO_ACCESS_KEY ? true : false),
    },
    {
        type: 'password',
        name: 'minioSecretKey',
        message: 'What is your MinIO secret key?',
        when: (answers) => (answers['mode'] === 'minio' && !MINIO_SECRET_KEY ? true : false),
    },
    {
        type: 'checkbox',
        name: 'documentTypes',
        message: 'What document types do you need? Select nothing if you want all types...',
        choices: [...PublicationNames, ...PublicationSubCategories],
    },
    {
        type: 'number',
        name: 'countLimit',
        message: 'How many documents are you looking for?',
        validate: (input) =>
            input > 0 && input <= 1000
                ? true
                : 'More than one, but no more than a thousand please!',
        default: 10,
    },
]

const answers = await inquirer.prompt(prompts)

if (
    answers['mode'] === 'minio' &&
    (!MINIO_HOST || !MINIO_PORT || !MINIO_ACCESS_KEY || !MINIO_SECRET_KEY)
) {
    const spinner = ora(`Creating a .env file for you...`).start()
    try {
        fs.writeFileSync(
            '.env',
            `MINIO_HOST="${answers['minioHost']}"
MINIO_PORT="${answers['minioPort']}"
MINIO_ACCESS_KEY="${answers['minioAccessKey']}" 
MINIO_SECRET_KEY="${answers['minioSecretKey']}"`
        )
        MINIO_HOST ??= answers['minioHost']
        MINIO_PORT ??= answers['minioPort']
        MINIO_ACCESS_KEY ??= answers['minioAccessKey']
        MINIO_SECRET_KEY ??= answers['minioSecretKey']
        spinner.succeed(`Created a .env file with your Minio credentials.`)
    } catch (error) {
        spinner.fail(`Could not create a .env file with your Minio credentials.`)
    }
}

const DocumentRecord = z.object({
    recordData: z.object({
        gzd: z.object({
            enrichedData: z.object({
                itemUrl: z.array(
                    z.object({
                        manifestation: z.string(),
                        $: z.string(),
                    })
                ),
            }),
            originalData: z.object({
                meta: z.object({
                    tpmeta: z.object({
                        publicatienaam: z.string(),
                    }),
                }),
            }),
        }),
    }),
})

type DocumentRecord = z.infer<typeof DocumentRecord>

const ResultSchema = z.object({
    searchRetrieveResponse: z.object({
        records: z.object({
            record: z.array(DocumentRecord),
        }),
    }),
})

type ResultSchema = z.infer<typeof ResultSchema>

const maximumRecordsPerPage = 10
const countLimit = answers['countLimit']
const selectedPublicationNames: string[] = answers['documentTypes'].filter((documentType: any) =>
    PublicationNames.includes(documentType)
)
const selectedPublicationSubCategories: string[] = answers['documentTypes'].filter(
    (documentType: any) => PublicationSubCategories.includes(documentType)
)

if (selectedPublicationNames.length === 0 && selectedPublicationSubCategories.length === 0) {
    selectedPublicationNames.push(...PublicationNames)
    selectedPublicationSubCategories.push(...PublicationSubCategories)
}

const publicationNameQuery =
    selectedPublicationNames.length > 0
        ? `(${selectedPublicationNames.map((p) => `w.publicatienaam=="${p}"`).join(')or(')})`
        : ''
const publicationSubCategoryQuery =
    selectedPublicationSubCategories.length > 0
        ? `(${selectedPublicationSubCategories.map((p) => `w.subrubriek=="${p}"`).join(')or(')})`
        : ''
const query =
    publicationNameQuery.length > 0 && publicationSubCategoryQuery.length > 0
        ? `((${publicationNameQuery})or(${publicationSubCategoryQuery}))`
        : publicationNameQuery.length > 0
          ? `(${publicationNameQuery})`
          : `(${publicationSubCategoryQuery})`

const pagination = got.paginate(
    `https://repository.overheid.nl/sru?&query=${query}&httpAccept=application/json&maximumRecords=${maximumRecordsPerPage}`,
    {
        pagination: {
            transform: (response: Response) => {
                if (response.request.options.responseType === 'json') {
                    return ResultSchema.parse(response.body).searchRetrieveResponse.records.record
                }

                return ResultSchema.parse(JSON.parse(response.body as string))
                    .searchRetrieveResponse.records.record
            },
            countLimit,
            paginate: ({ response }) => {
                try {
                    const nextRecordPosition = JSON.parse(response.body as string)
                        .searchRetrieveResponse.nextRecordPosition
                    const nextPageUrl = new URL(response.url)
                    nextPageUrl.searchParams.set('startRecord', nextRecordPosition)
                    return {
                        url: nextPageUrl,
                    }
                } catch (err) {
                    return false
                }
            },
            backoff: 250,
        },
    }
)

const minioClient = new minio.Client({
    endPoint: MINIO_HOST ?? 'localhost',
    port: parseInt(MINIO_PORT ?? '9000'),
    useSSL: false,
    accessKey: MINIO_ACCESS_KEY ?? 'unknown',
    secretKey: MINIO_SECRET_KEY ?? 'unknown',
})

const records: DocumentRecord[] = []
for await (const document of pagination) {
    const pdfManifestation = document.recordData.gzd.enrichedData.itemUrl.find(
        (item: { manifestation: string }) => item.manifestation === 'pdf'
    )
    if (pdfManifestation) {
        const pdfUrl = new URL(pdfManifestation.$)
        const pdfFileName = path.basename(pdfUrl.pathname)
        const publicationType = document.recordData.gzd.originalData.meta.tpmeta.publicatienaam
        const downloadSubDir = `${downloadDir}/${publicationType}`
        const fileDownloadPath = `${downloadSubDir}/${pdfFileName}`
        if (answers['mode'] === 'minio') {
            const spinner = ora(`Uploading ${pdfFileName}...`).start()
            try {
                if (fs.existsSync(fileDownloadPath)) {
                    spinner.text = `Uploading ${pdfFileName} from local cache...`
                    await minioClient.putObject(
                        'files',
                        pdfFileName,
                        fs.createReadStream(fileDownloadPath)
                    )
                    spinner.succeed(
                        `Uploaded ${pdfFileName}... (${publicationType}) from local cache`
                    )
                } else {
                    await minioClient.putObject('files', pdfFileName, got.stream.get(pdfUrl))
                    spinner.succeed(`Uploaded ${pdfFileName}... (${publicationType})`)
                }
            } catch (error) {
                spinner.fail(`Could not upload ${pdfFileName} (${publicationType})`)
            }
        } else if (answers['mode'] === 'download') {
            if (!fs.existsSync(downloadSubDir)) {
                fs.mkdirSync(downloadSubDir, { recursive: true })
            }
            const spinner = ora(`Downloading ${pdfFileName}...`).start()

            if (fs.existsSync(fileDownloadPath)) {
                spinner.warn(`Skipped downloading ${pdfFileName} because it already exists...`)
                continue
            }

            try {
                await streamPipeline(got.stream(pdfUrl), fs.createWriteStream(fileDownloadPath))
                spinner.succeed(`Downloaded ${pdfFileName}... (${publicationType})`)
            } catch (error) {
                spinner.fail(`Could not download ${pdfFileName} (${publicationType})`)
                console.error(error)
            }
        }
    }
}

process.exit()
